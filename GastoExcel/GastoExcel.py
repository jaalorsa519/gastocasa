from datetime import datetime
import openpyxl

#Programa que guarda los gastos en un archivo xls
ruta='ContabilidadCasa.xlsx'
fichero=open(ruta,'br')
lb=openpyxl.load_workbook(fichero)
hj=lb.active
maxFila=hj.max_row
hoy=datetime.now()
concepto=input('Concepto\n')
cantidad=int(input('Cantidad\n'))
valor=int(input('Valor\n'))
#fecha-concepto-cantidad-valor-total
dato=((hoy.date()),concepto,cantidad,valor,f'=c{maxFila+1}*d{maxFila+1}')
hj.append(dato)
lb.save(ruta)
